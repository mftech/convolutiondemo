/*****************************************************************************************
* Author: Ed Ang
* Date: 6 March 2020
* Header file for data structures.
******************************************************************************************/

struct darray_1d_int_type {
    int* data;
    int* time;
    int length;
};

struct darray_2d_int_type {
    int** data;
    int* time;
    int width;
    int height;
};

class dataStructures 
{
    
public:
    
    darray_1d_int_type init_ds_1d(int* arr, int start_time, int len) {
        darray_1d_int_type ds;
        int *ds_array;
        ds_array = new int[len];
        int *time_array;
        time_array = new int[len];
        
        // Copy over the array.
        for(int i=0; i<len; i++) {
            ds_array[i] = arr[i];
            time_array[i] = start_time+i;
        }
        
        // Make assignments.
        ds.data = ds_array;
        ds.time = time_array;
        ds.length = len;
        return(ds);
    }
    
    darray_1d_int_type init_ds_1d(int start_time, int len) {
        darray_1d_int_type ds;
        int *ds_array;
        ds_array = new int[len];
        int *time_array;
        time_array = new int[len];
        
        // Copy over the array.
        for(int i=0; i<len; i++) {
            ds_array[i] = 0;
            time_array[i] = start_time+i;
        }
        // Make assignments.
        ds.data = ds_array;
        ds.time = time_array;
        ds.length = len;
        return(ds);
    }
    
    darray_2d_int_type init_ds_2d(int start_time, int wd, int ht) {
        darray_2d_int_type ds;
        int i;
        int *time_array;
        time_array = new int[wd];
        
        // Create a time array.
        for(i=0; i<wd; i++) {
            time_array[i] = start_time+i;
        }
        
        // Create a 2D array.
        int** ds_array = new int*[ht];
        for(i = 0; i < ht; ++i) {
            ds_array[i] = new int[wd];
        }
        
        // Make assignments.
        ds.data = ds_array;
        ds.time = time_array;
        ds.width = wd;
        ds.height = ht;
        return(ds);
    }
    
    
    std::vector<int> oneDInt2Vector(int* ds, int length) {
        std::vector<int> vec(ds, ds+length);
        return(vec);
    }
    
    std::vector<int> selectRow(darray_2d_int_type ds, int row) {
        int* current_row = new int[ds.width];
        
        // Copy over the appropriate row.
        for(int i=0; i<ds.width; i++) {
            current_row[i] = ds.data[row][i];
        }
        
        std::vector<int> vec(current_row, current_row+ds.width);
        return(vec);
    }
    
    void write_ds(darray_1d_int_type x, int index, int element) {
        
        // Find the position corresponding to the index.
        int pos = find(x.time, x.length, index);
        
        // Write element to position.
        if(pos != -1) {
            if(log_data)
                std::cout<<"data_structures.hpp:write_ds:Index "<<index<<", Value "<<x.data[pos]<<"\n";
            x.data[pos] = element;
        } else {
            if(log_data)
                std::cout<<"data_structures.hpp:write_ds:Index "<<index<<" CANNOT be found!\n";
        }
    }
    
    int read_ds(darray_1d_int_type x, int index) {
        
        // Find the position corresponding to the index.
        int pos = find(x.time, x.length, index);
        
        // Read element from array.
        if(pos != -1) {
            if(log_data)
                std::cout<<"data_structures.hpp:read_ds:Index "<<index<<", Value "<<x.data[pos]<<"\n";
            return(x.data[pos]);
        } else {
            if(log_data)
                std::cout<<"data_structures.hpp:read_ds:Index "<<index<<" CANNOT be found!\n";
            return(0);
        }
    }
    
private:
    
    int find(int* arr, int length, int element) {
        
        for(int i=0; i<length; i++) {
            if(arr[i] == element) {
                return i;
            }
        }
        
        return -1;
    }
    
    bool log_data = false;
};

