/*****************************************************************************************
* Author: Ed Ang
* Date: 12 March 2020
* Header file for the convolution class.
******************************************************************************************/

#include "xwidgets/xbutton.hpp"
#include "xwidgets/xoutput.hpp"
#include "xwidgets/xbox.hpp"

#pragma cling add_include_path("/opt/conda/include/python3.7m")
#pragma cling add_include_path("/opt/conda/lib/python3.7/site-packages/numpy/core/include")
#pragma cling add_include_path("matplotlib-cpp")
#pragma cling load("/opt/conda/lib/libpython3.7m.so.1.0")

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <matplotlibcpp.h>
#include <string>
#include <fstream>
#include <algorithm>
#include <xcpp/xdisplay.hpp>
#include "nlohmann/json.hpp"
#include "xtl/xbase64.hpp"

#include "dataStructures.hpp"
#include "convolution.hpp"
#include "display.hpp"

darray_1d_int_type s1_ds, s2_ds;

darray_1d_int_type reflected;
darray_2d_int_type s1_shifted;
darray_1d_int_type convolved;
int num_shifts;

// Widgets to activate signal animation.
xw::button button;
xw::output xout;

darray_1d_int_type multi_dim_signals[2];
std::string signal_labels[2];

convolution convolver;
display displayer;
dataStructures ds;
