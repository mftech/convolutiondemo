/*****************************************************************************************
* Author: Ed Ang
* Date: 5 March 2020
* Header file for the convolution class.
******************************************************************************************/

namespace nl = nlohmann;
namespace plt = matplotlibcpp;

namespace im
{
    struct image
    {   
        inline image(const std::string& filename)
        {
            std::ifstream fin(filename, std::ios::binary);   
            m_buffer << fin.rdbuf();
        }
            
        std::stringstream m_buffer;
    };
        
    nl::json mime_bundle_repr(const image& i)
    {
        auto bundle = nl::json::object();
        bundle["image/png"] = xtl::base64encode(i.m_buffer.str());
        return bundle;
    }
}

namespace ht
{
    struct html
    {   
        inline html(const std::string& content)
        {
            m_content = content;
        }
        std::string m_content;
    };

    nl::json mime_bundle_repr(const html& a)
    {
        auto bundle = nl::json::object();
        bundle["text/html"] = a.m_content;
        return bundle;
    }
}

class display
{
public:
    int fig_wd, fig_ht;
    dataStructures ds;
    convolution convolver;
    std::map<std::string, std::string> chart_params;
    
    
    display() {
        fig_wd = 600;
        fig_ht = 390;
    }
    
    void show_video() {
        ht::html rect(R"(<video width="100%" controls> <source src="video/convolution/convolution1.mp4" type="video/mp4"> </video>)");
        xcpp::display(rect, "some_display_id");
    }
    
    void plot_signal(const std::string label, darray_1d_int_type y) {
        
        // Convert ds type to vector format.
        std::vector<int> xvec = ds.oneDInt2Vector(&y.time[0], y.length);
        std::vector<int> yvec = ds.oneDInt2Vector(&y.data[0], y.length);
        
        plt::figure_size(fig_wd*2, fig_ht);
        plt::stem(xvec, yvec);
        plt::title(label);
        plt::legend();
        
        const char* filename = "Charts/current_chart.png";
        plt::save(filename);
        
        im::image signal(filename);
        xcpp::display(signal);
    }
    
    void plot_signals(std::string labels[2], darray_1d_int_type y[2]) {
        
        // Convert ds type to vector format.
        std::vector<int> xvec1 = ds.oneDInt2Vector(&y[0].time[0], y[0].length);
        std::vector<int> yvec1 = ds.oneDInt2Vector(&y[0].data[0], y[0].length);
        
        std::vector<int> xvec2 = ds.oneDInt2Vector(&y[1].time[0], y[1].length);
        std::vector<int> yvec2 = ds.oneDInt2Vector(&y[1].data[0], y[1].length);
        
        plt::figure_size(fig_wd*2, fig_ht);
        plt::subplot(1,2,1);
        plt::stem(xvec1, yvec1);
        plt::title(labels[0]);
        
        plt::subplot(1,2,2);
        plt::stem(xvec2, yvec2);
        plt::title(labels[1]);
        
        const char* filename = "Charts/current_chart.png";
        plt::save(filename);
        
        im::image signal(filename);
        xcpp::display(signal);
    }
    
    void animate_signal(const std::string label, darray_1d_int_type x_array, darray_2d_int_type y_matrix) {
        
        std::vector<int> xvec = ds.oneDInt2Vector(&x_array.data[0], x_array.length);
        int i = 0;
        
        for(int i=0; i<y_matrix.height; ++i) {
                          
            std::vector<int> yvec = ds.selectRow(y_matrix, i);
            
            plt::figure_size(fig_wd, fig_ht);
            plt::stem(xvec, yvec);
            plt::title(label);
            plt::legend();
            
            const char* filename = "Charts/current_chart.png";
            plt::save(filename);
            
            im::image signal(filename);
            xcpp::display(signal);
            
            xcpp::clear_output(true);
            sleep(1);
        }
        
    }
    
    void demo_convolution(const std::string label, darray_1d_int_type signals[2]) {
        darray_1d_int_type signal_one = signals[0];
        darray_1d_int_type signal_two = signals[1];
        
        darray_1d_int_type convolved = ds.init_ds_1d(signal_one.time[0], signal_one.length);
        
        // Reflect signal_one about the y-axis.
        darray_1d_int_type signal_one_reflected = convolver.reflect_y(signal_one);
        
        std::vector<int> xvec = ds.oneDInt2Vector(&signal_one.time[0], signal_one.length);
        
        for(int i=0; i<signal_one.length; i++) {
            std::vector<int> yvec1 = ds.oneDInt2Vector(&signal_one_reflected.data[0], signal_one_reflected.length);
            std::vector<int> yvec2 = ds.oneDInt2Vector(&signal_two.data[0], signal_two.length);
            
            // Obtained the convolved result at index i.
            int dot = convolver.dot_product(signal_one_reflected, signal_two);
            //convolved.data[i] = dot;
            ds.write_ds(convolved, i, dot);
            std::vector<int> yvec3 = ds.oneDInt2Vector(&convolved.data[0], convolved.length);
            
            plt::figure_size(fig_wd*2, fig_ht*2);
            plt::subplot(2,1,1);
            plt::stem(xvec, yvec1, 0);
            plt::stem(xvec, yvec2, 1);
            plt::title(label);
            
            plt::subplot(2,1,2);
            plt::stem(xvec, yvec3, 1);
            
            const char* filename = "Charts/current_chart.png";
            plt::save(filename);
            
            im::image signal(filename);
            xcpp::display(signal);
            
            // Shift the signal to the right.
            convolver.shift_right(&signal_one_reflected);
            
            sleep(1);
            
            if(i<signal_one.length-1) {
                xcpp::clear_output(true);
            }
        }
        
    }
    
    void foo()
    {
        std::cout << "Clicked!" << std::endl;
    }
    
};