/*****************************************************************************************
* Author: Ed Ang
* Date: 4 March 2020
* Header file for the convolution class.
******************************************************************************************/

class convolution
{
public:
    int signal_sz;
    std::vector<int> x, s1, s2;
    dataStructures ds;
    
    convolution() {
        
        // Initialise the signal variables.
        signal_sz = 8;
    }
    
    
    // Reflect the signal about the y-axis (negation of the x coordinates).
    darray_1d_int_type reflect_y(darray_1d_int_type x) {
        
        darray_1d_int_type reflected = ds.init_ds_1d(x.time[0], x.length);
        
        for(int index=0; index<x.length; index++) {
            
            // Find array index corresponding to the relected index.
            int itr = find(x.time, x.length, -x.time[index]);
            
            if(itr != -1) {
                reflected.data[itr] = x.data[index];
                reflected.data[index] = x.data[itr];
            } else {
                reflected.data[index] = 0;
            }
        }
        std::cout<<std::endl;
        return(reflected);
    }
    
    
    void shift_right(darray_1d_int_type* x) {
        int* temp;
        temp = new int[x->length];
        int i;
        
        // Buffer input array.
        for(i=0; i<x->length; i++) {
            temp[i] = x->data[i];
        }
        
        // Copy over input array to the first row of the matrix.
        for(i=1; i<x->length; i++) {
             x->data[i] = temp[i-1];
        }
        x->data[0] = 0;
        
        // Deallocate array.
        delete temp;
    }
    
    int dot_product(darray_1d_int_type x1, darray_1d_int_type x2) {
        int result = 0;
        
        for(int i=0; i<x1.length; i++) {
            result += x1.data[i]*x2.data[i];
        }
        
        return(result);
    }
    
    
private:
    int member;
    
    int find(int* arr, int length, int element) {
        
        for(int i=0; i<length; i++) {
            if(arr[i] == element) {
                return i;
            }
        }
        
        return -1;
    }
    
};